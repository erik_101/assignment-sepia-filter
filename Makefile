.PHONY: clean

#-ggdb
CFLAGS= -std=c17 -pedantic -Wall -O3 -g -c

CC = gcc
LINKER = $(CC)
NASM = nasm

SOLUTION = solution

SRC = $(SOLUTION)/src
OBJECT = obj
TARGET = build

MKDIR = mkdir -p


all: $(OBJECT) $(TARGET)
	$(NASM) -felf64 -g $(SRC)/sepia_filter_asm.asm -o $(OBJECT)/sepia_filter_asm.o

	$(CC) $(CFLAGS) $(SRC)/bmp_handler.c -o $(OBJECT)/bmp_handler.o
	$(CC) $(CFLAGS) $(SRC)/file_manager.c -o $(OBJECT)/file_manager.o
	$(CC) $(CFLAGS) $(SRC)/utils/image.c -o $(OBJECT)/image.o

	$(CC) $(CFLAGS) $(SRC)/rotator.c -o $(OBJECT)/rotator.o
	$(CC) $(CFLAGS) $(SRC)/sepia_filter.c -o $(OBJECT)/sepia_filter.o

	$(CC) $(CFLAGS) $(SRC)/main.c -o $(OBJECT)/main.o

	$(LINKER) -g -no-pie $(OBJECT)/*.o -o  $(TARGET)/main

$(OBJECT):
	$(MKDIR) $@

$(TARGET):
	$(MKDIR) $@

clean:
	rm -rf $(OBJECT) $(TARGET)

run_0:
	./$(TARGET)/main ./$(TARGET)/input.bmp ./$(TARGET)/output.bmp 0

run_1:
	./$(TARGET)/main ./$(TARGET)/input.bmp ./$(TARGET)/output.bmp 1
