#ifndef LAB3___2_BMP_HANDLER_H
#define LAB3___2_BMP_HANDLER_H

#include "image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include  <stdint.h>
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum READ_STATUS {
    READ_SUCCESS = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum WRITE_STATUS {
    WRITE_SUCCESS = 0,
    WRITE_ERROR
};

enum READ_STATUS from_bmp(FILE *in, struct image *image);

enum WRITE_STATUS to_bmp(FILE *out, const struct image *image);


#endif //LAB3___2_BMP_HANDLER_H
