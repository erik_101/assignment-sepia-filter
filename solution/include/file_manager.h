#ifndef LAB3___2_FILE_MANAGER_H
#define LAB3___2_FILE_MANAGER_H

#include <stdio.h>
#include <stdlib.h>

enum OPEN_STATUS {
    OPEN_SUCCESS,
    OPEN_ERROR
};

enum CLOSE_STATUS {
    CLOSE_SUCCESS,
    CLOSE_ERROR
};

enum OPEN_STATUS open_file(FILE **file, const char *filename, const char *mode);
enum CLOSE_STATUS close_file(FILE *file);

#endif //LAB3___2_FILE_MANAGER_H
