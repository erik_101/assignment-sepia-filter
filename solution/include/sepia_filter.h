#ifndef ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H
#define ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H

#include "image.h"
#include <inttypes.h>

struct image sepia_filter_c_impl(struct image source);
struct image sepia_filter_asm_impl(struct image source);

#endif //ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H
