#ifndef LAB3___2_IMAGE_H
#define LAB3___2_IMAGE_H

#include "stdint.h"
#include <malloc.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image get_image(uint64_t width, uint64_t height, struct pixel *data);

struct image create_image(uint64_t width, uint64_t height);

struct image image_copy(struct image image);

void destroy_image(struct image *image);

#endif //LAB3___2_IMAGE_H
