#include "../include/file_manager.h"
#include "../include/bmp_handler.h"
#include "../include/sepia_filter.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    if (argc < 4) {
        fprintf(stderr, "Invalid number of arguments!\n"
                        "Expected: <source-image>\n"
                        "          <transformed-image-1>\n"
                        "          <transform-mode>\n");
        return -1;
    }

    FILE *input_file;

    if (open_file(&input_file, argv[1], "r") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open input file for read!");
        return -1;
    }

    struct image source_image = {0};

    if (from_bmp(input_file, &source_image) != READ_SUCCESS) {
        fprintf(stderr, "Unable to read image from input file!");
        return -1;
    }

    close_file(input_file);

    struct rusage r;
    struct timeval start;
    struct timeval end;

    getrusage(RUSAGE_SELF, &r );
    start = r.ru_utime;

    struct image filtered_image;

    if (*argv[3] == '0') {
        filtered_image = sepia_filter_c_impl(source_image);
    } else if (*argv[3] == '1') {
        filtered_image = sepia_filter_asm_impl(source_image);
    } else {
        fprintf(stderr, "Error transform mode code!");
        return -1;
    }

    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;

    long res = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    if (*argv[3] == '1') {
        fprintf(stdout, "Time elapsed in microseconds, Assembly program: %ld\n", res);
    } else {
        fprintf(stdout, "Time elapsed in microseconds, C program: %ld\n", res);
    }

    FILE *output_file;

    if (open_file(&output_file, argv[2], "w") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open output file for write!");
        return -1;
    }

    if (to_bmp(output_file, &filtered_image) != WRITE_SUCCESS) {
        fprintf(stderr, "Unable to read image from input file!");
        return -1;
    }

    close_file(output_file);

    destroy_image(&source_image);
    destroy_image(&filtered_image);

    return 0;
}