#include "../../include/image.h"

struct image get_image(uint64_t width, uint64_t height, struct pixel *data) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = data
    };
}

struct image create_image(uint64_t width, uint64_t height) {

    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);

    return get_image(width, height, pixels);
}


struct image image_copy(struct image image) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * image.width * image.height);

    for (size_t i = 0; i < image.height * image.width; i++) {
        pixels[i] = image.data[i];
    }

    return get_image(image.width, image.height, pixels);
}


void destroy_image(struct image *image) {
    free(image->data);
}
