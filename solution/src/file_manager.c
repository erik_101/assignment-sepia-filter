#include "../include/file_manager.h"

enum OPEN_STATUS open_file(FILE **file, const char *filename, const char *mode) {
    *file = fopen(filename, mode);

    if (*file == NULL) {
        return OPEN_ERROR;
    }

    return OPEN_SUCCESS;
}

enum CLOSE_STATUS close_file(FILE *file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }

    return CLOSE_SUCCESS;
}